//Basic Data Structures

//Arrays
 val numbers = Array(1, 2, 3, 4, 5, 1, 2, 3, 4, 5)
//numbers: Array[Int] = Array(1, 2, 3, 4, 5, 1, 2, 3, 4, 5)

numbers(3) = 10

//Lists
val numbers = List(1, 2, 3, 4, 5, 1, 2, 3, 4, 5)
//numbers: List[Int] = List(1, 2, 3, 4, 5, 1, 2, 3, 4, 5)

/**
numbers(3) = 10
<console>:9: error: value update is not a member of List[Int]
 numbers(3) = 10
 */

//Sets
val numbers = Set(1, 2, 3, 4, 5, 1, 2, 3, 4, 5)
//numbers: scala.collection.immutable.Set[Int] = Set(5, 1, 2, 3, 4)

//Tuple
val hostPort = ("localhost", 80)
//hostPort: (String, Int) = (localhost, 80)

hostPort._1
//res0: String = localhost

hostPort._2
//res1: Int = 80

/**
hostPort match {
 case ("localhost", port) => ...
 case (host, port) => ...
}
 */
1 -> 2
//res0: (Int, Int) = (1,2)


//Maps
Map(1 -> 2)
Map("foo" -> "bar")

Map(1 -> Map("foo" -> "bar"))
//Map("timesTwo" -> { timesTwo(_) })


//Option
trait Option[T] {
 def isDefined: Boolean
 def get: T
 def getOrElse(t: T): T
}

val numbers = Map("one" -> 1, "two" -> 2)
//numbers: scala.collection.immutable.Map[java.lang.String,Int] = Map(one -> 1, two -> 2)

numbers.get("two")
//res7: Option[Int] = Some(2)

numbers.get("three")
//res8: Option[Int] = None

// We want to multiply the number by two, otherwise return 0.
val result = if (res8.isDefined) {
 res8.get * 2
} else {
 7
}

val result = res8.getOrElse(7) * 2

val result = res8 match {
 case Some(n) => n * 2
 case None => 7
}


//Functional Combinators

//map
val numbers = List(1, 2, 3, 4)
//numbers: List[Int] = List(1, 2, 3, 4)

numbers.map((i: Int) => i * 2)
//res0: List[Int] = List(2, 4, 6, 8)

//or pass in a function (the Scala compiler automatically converts our method to a function)

def timesTwo(i: Int): Int = i * 2
//timesTwo: (i: Int)Int

numbers.map(timesTwo)
//res0: List[Int] = List(2, 4, 6, 8)


//foreach
numbers.foreach((i: Int) => i * 2)

val doubled = numbers.foreach((i: Int) => i * 2)
//doubled: Unit = ()


//filter
numbers.filter((i: Int) => i % 2 == 0)
//res0: List[Int] = List(2, 4)

def isEven(i: Int): Boolean = i % 2 == 0
//isEven: (i: Int)Boolean

numbers.filter(isEven)
//res2: List[Int] = List(2, 4)


//zip
List(1, 2, 3).zip(List("a", "b", "c"))
//res0: List[(Int, String)] = List((1,a), (2,b), (3,c))


//partition
val numbers = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
numbers.partition(_ % 2 == 0)
//res0: (List[Int], List[Int]) = (List(2, 4, 6, 8, 10),List(1, 3, 5, 7, 9))


//find
numbers.find((i: Int) => i > 5)
//res0: Option[Int] = Some(6)


//drop & dropWhile
numbers.drop(5)
//res0: List[Int] = List(6, 7, 8, 9, 10)

numbers.dropWhile(_ % 2 != 0)
//res0: List[Int] = List(2, 3, 4, 5, 6, 7, 8, 9, 10)


//foldLeft
numbers.foldLeft(0)((m: Int, n: Int) => m + n)
//res0: Int = 55
/**
numbers.foldLeft(0) { (m: Int, n: Int) => println("m: " + m + " n: " + n); m + n }
m: 0 n: 1
m: 1 n: 2
m: 3 n: 3
m: 6 n: 4
m: 10 n: 5
m: 15 n: 6
m: 21 n: 7
m: 28 n: 8
m: 36 n: 9
m: 45 n: 10
//res0: Int = 55
*/

/**
foldRight

numbers.foldRight(0) { (m: Int, n: Int) => println("m: " + m + " n: " + n); m + n }
m: 10 n: 0
m: 9 n: 10
m: 8 n: 19
m: 7 n: 27
m: 6 n: 34
m: 5 n: 40
m: 4 n: 45
m: 3 n: 49
m: 2 n: 52
m: 1 n: 54
//res0: Int = 55
*/

//flatten
List(List(1, 2), List(3, 4)).flatten
//res0: List[Int] = List(1, 2, 3, 4)

//flatMap
val nestedNumbers = List(List(1, 2), List(3, 4))
//nestedNumbers: List[List[Int]] = List(List(1, 2), List(3, 4))

nestedNumbers.flatMap(x => x.map(_ * 2))
//res0: List[Int] = List(2, 4, 6, 8)

nestedNumbers.map((x: List[Int]) => x.map(_ * 2)).flatten
//res1: List[Int] = List(2, 4, 6, 8)


//Generalized functional combinators
def ourMap(numbers: List[Int], fn: Int => Int): List[Int] = {
 numbers.foldRight(List[Int]()) { (x: Int, xs: List[Int]) =>
  fn(x) :: xs
 }
}

ourMap(numbers, timesTwo(_))
//res0: List[Int] = List(2, 4, 6, 8, 10, 12, 14, 16, 18, 20)


//Map?
val extensions = Map("steve" -> 100, "bob" -> 101, "joe" -> 201)
//extensions: scala.collection.immutable.Map[String,Int] = Map((steve,100), (bob,101), (joe,201))

extensions.filter((namePhone: (String, Int)) => namePhone._2 < 200)
//res0: scala.collection.immutable.Map[String,Int] = Map((steve,100), (bob,101))

 extensions.filter({case (name, extension) => extension < 200})
//res0: scala.collection.immutable.Map[String,Int] = Map((steve,100), (bob,101))
