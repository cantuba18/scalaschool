//Function Composition
def f(s: String) = "f(" + s + ")"
//f: (String)java.lang.String

def g(s: String) = "g(" + s + ")"
//g: (String)java.lang.String

//compose
val fComposeG = f _ compose g _
//fComposeG: (String) => java.lang.String = <function>

fComposeG("yay")
//res0: java.lang.String = f(g(yay))

//andThen
val fAndThenG = f _ andThen g _
//fAndThenG: (String) => java.lang.String = <function>

fAndThenG("yay")
//res1: java.lang.String = g(f(yay))


//Currying vs Partial Application

//Understanding PartialFunction
val one: PartialFunction[Int, String] = { case 1 => "one" }
//one: PartialFunction[Int,String] = <function1>

one.isDefinedAt(1)
//res0: Boolean = true

one.isDefinedAt(2)
//res1: Boolean = false

one(1)
//res2: String = one

val two: PartialFunction[Int, String] = { case 2 => "two" }
//two: PartialFunction[Int,String] = <function1>

val three: PartialFunction[Int, String] = { case 3 => "three" }
//three: PartialFunction[Int,String] = <function1>

val wildcard: PartialFunction[Int, String] = { case _ => "something else" }
//wildcard: PartialFunction[Int,String] = <function1>

val partial = one orElse two orElse three orElse wildcard
//partial: PartialFunction[Int,String] = <function1>

partial(5)
//res24: String = something else

 partial(3)
//res25: String = three

partial(2)
//res26: String = two

partial(1)
//res27: String = one

partial(0)
//res28: String = something else


//The mystery of case.
case class PhoneExt(name: String, ext: Int)
//defined class PhoneExt

val extensions = List(PhoneExt("steve", 100), PhoneExt("robey", 200))
//extensions: List[PhoneExt] = List(PhoneExt(steve,100), PhoneExt(robey,200))

extensions.filter { case PhoneExt(name, extension) => extension < 200 }
//res0: List[PhoneExt] = List(PhoneExt(steve,100))

