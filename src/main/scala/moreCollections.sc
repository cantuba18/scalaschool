//List
List(1, 2, 3)
//res0: List[Int] = List(1, 2, 3)

1 :: 2 :: 3 :: Nil
//res1: List[Int] = List(1, 2, 3)

//Set
Set(1, 1, 2)
//res2: scala.collection.immutable.Set[Int] = Set(1, 2)

//Seq
Seq(1, 1, 2)
//res3: Seq[Int] = List(1, 1, 2)

//Map
Map('a' -> 1, 'b' -> 2)
//res4: scala.collection.immutable.Map[Char,Int] = Map((a,1), (b,2))


//Traversable

Map(1 -> 2).toArray
//res41: Array[(Int, Int)] = Array((1,2))

//Iterable
def iterator: Iterator[A]

def hasNext(): Boolean
def next(): A


//Set
def contains(key: A): Boolean
def +(elem: A): Set[A]
def -(elem: A): Set[A]

//Map
Map("a" -> 1, "b" -> 2)
//res0: scala.collection.immutable.Map[java.lang.String,Int] = Map((a,1), (b,2))

Map(("a", 2), ("b", 2))
//res0: scala.collection.immutable.Map[java.lang.String,Int] = Map((a,2), (b,2))


//Commonly-used subclasses
IndexedSeq(1, 2, 3)
//res0: IndexedSeq[Int] = Vector(1, 2, 3)

(1 to 3).map { i => i }
//res0: scala.collection.immutable.IndexedSeq[Int] = Vector(1, 2, 3)