//Quantification
def count[A](l: List[A]) = l.size
//count: [A](List[A])Int

def count(l: List[_]) = l.size
//count: (List[_])Int

def count(l: List[T forSome { type T }]) = l.size
//count: (List[T forSome { type T }])Int

def drop1(l: List[_]) = l.tail
//drop1: (List[_])List[Any]

def drop1(l: List[T forSome { type T }]) = l.tail
//drop1: (List[T forSome { type T }])List[T forSome { type T }]

def hashcodes(l: Seq[_ <: AnyRef]) = l map (_.hashCode)
//hashcodes: (Seq[_ <: AnyRef])Seq[Int]


/**
hashcodes(Seq(1,2,3))

<console>:7: error: type mismatch;
  found   : Int(1)
  required: AnyRef
  Note: primitive types are not implicitly converted to AnyRef.
  You can safely force boxing by casting x.asInstanceOf[AnyRef].
  hashcodes(Seq(1,2,3))


  scala> hashcodes(Seq("one", "two", "three"))
  res1: Seq[Int] = List(110182, 115276, 110339486)
 */


//Parametric polymorphism
2 :: 1 :: "bar" :: "foo" :: Nil
//res5: List[Any] = List(2, 1, bar, foo)

res0.head
//res6: Any = 2

def drop1[A](l: List[A]) = l.tail
//drop1: [A](l: List[A])List[A]

drop1(List(1,2,3))
//res1: List[Int] = List(2, 3)


//Scala has rank-1 polymorphism
def toList[A](a: A) = List(a)

/**
which you wished to use generically:
def foo[A, B](f: A => List[A], b: B) = f(b)

This does not compile, because all type variables have to be fixed at the invocation site. Even if you “nail down” type B,
def foo[A](f: A => List[A], i: Int) = f(i)

…you get a type mismatch.

Type inference

{ x => x }
<console>:7: error: missing parameter type
  { x => x }

 Whereas in OCaml, you can:
 fun x -> x;;
- : 'a -> 'a = <fun>
 */

//In scala all type inference is local. Scala considers one expression at a time. For example:
def id[T](x: T) = x
//id: [T](x: T)T

 val x = id(322)
//x: Int = 322

val x = id("hey")
//x: java.lang.String = hey

val x = id(Array(1,2,3,4))
//x: Array[Int] = Array(1, 2, 3, 4)


//Variance
class Covariant[+A]
//defined class Covariant

val cv: Covariant[AnyRef] = new Covariant[String]
//cv: Covariant[AnyRef] = Covariant@4035acf6

/**
val cv: Covariant[String] = new Covariant[AnyRef]

<console>:6: error: type mismatch;
  found   : Covariant[AnyRef]
  required: Covariant[String]
  val cv: Covariant[String] = new Covariant[AnyRef]
 */

class Contravariant[-A]
//defined class Contravariant

val cv: Contravariant[String] = new Contravariant[AnyRef]
//cv: Contravariant[AnyRef] = Contravariant@49fa7ba

/**
val fail: Contravariant[AnyRef] = new Contravariant[String]

<console>:6: error: type mismatch;
  found   : Contravariant[String]
  required: Contravariant[AnyRef]
  val fail: Contravariant[AnyRef] = new Contravariant[String]
  */

trait Function1 [-T1, +R] extends AnyRef

class Animal { val sound = "rustle" }
//defined class Animal

class Bird extends Animal { override val sound = "call" }
//defined class Bird

class Chicken extends Bird { override val sound = "cluck" }
//defined class Chicken

val getTweet: (Bird => String) =  //TODO

val getTweet: (Bird => String) = ((a: Animal) => a.sound )
//getTweet: Bird => String = <function1>

val hatch: (() => Bird) = (() => new Chicken )
//hatch: () => Bird = <function0>


/**
Bounds

 def cacophony[T](things: Seq[T]) = things map (_.sound)
<console>:7: error: value sound is not a member of type parameter T
       def cacophony[T](things: Seq[T]) = things map (_.sound)

def biophony[T <: Animal](things: Seq[T]) = things map (_.sound)
biophony: [T <: Animal](things: Seq[T])Seq[java.lang.String]

biophony(Seq(new Chicken, new Bird))
res5: Seq[java.lang.String] = List(cluck, call)
 */

val flock = List(new Bird, new Bird)
//flock: List[Bird] = List(Bird@7e1ec70e, Bird@169ea8d2)

new Chicken :: flock
//res53: List[Bird] = List(Chicken@56fbda05, Bird@7e1ec70e, Bird@169ea8d2)

new Animal :: flock
//res59: List[Animal] = List(Animal@11f8d3a8, Bird@7e1ec70e, Bird@169ea8d2)


