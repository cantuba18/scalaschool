//apply methods
class Foo {}
//defined class Foo

object FooMaker {
     def apply() = new Foo
   }
//defined module FooMaker

val newFoo = FooMaker()
//newFoo: Foo = Foo@5b83f762

//or

class Bar {
     def apply() = 0
   }
//defined class Bar

val bar = new Bar
//bar: Bar = Bar@47711479

bar()
//res8: Int = 0


//Objects
object Timer {
  var count = 0

  def currentCount(): Long = {
    count += 1
    count
  }
}

Timer.currentCount()
//res0: Long = 1

class Bar(foo: String)

object Bar {
  def apply(foo: String) = new Bar(foo)
}


//Functions are Objects
object addOne extends Function1[Int, Int] {
     def apply(m: Int): Int = m + 1
   }
//defined module addOne

 addOne(1)
//res2: Int = 2

class AddOne extends Function1[Int, Int] {
     def apply(m: Int): Int = m + 1
   }
//defined class AddOne

val plusOne = new AddOne()
//plusOne: AddOne = <function1>

plusOne(1)
//res0: Int = 2

class AddOne extends (Int => Int) {
  def apply(m: Int): Int = m + 1
}

/**Packages
package com.twitter.example

object colorHolder {
  val BLUE = "Blue"
  val RED = "Red"
}

println("the color is: " + com.twitter.example.colorHolder.BLUE)
*/

object colorHolder {
     val Blue = "Blue"
     val Red = "Red"
   }
//defined module colorHolder


//Pattern Matching
val times = 1

times match {
  case 1 => "one"
  case 2 => "two"
  case _ => "some other number"
}

times match {
  case i if i == 1 => "one"
  case i if i == 2 => "two"
  case _ => "some other number"
}


//Matching on type
def bigger(o: Any): Any = {
  o match {
    case i: Int if i < 0 => i - 1
    case i: Int => i + 1
    case d: Double if d < 0.0 => d - 0.1
    case d: Double => d + 0.1
    case text: String => text + "s"
  }
}


/**
Matching on class members

def calcType(calc: Calculator) = calc match {
  case _ if calc.brand == "HP" && calc.model == "20B" => "financial"
  case _ if calc.brand == "HP" && calc.model == "48G" => "scientific"
  case _ if calc.brand == "HP" && calc.model == "30B" => "business"
  case _ => "unknown"
}
 */


//Case Classes
case class Calculator(brand: String, model: String)
//defined class Calculator

val hp20b = Calculator("HP", "20b")
//hp20b: Calculator = Calculator(hp,20b)

val hp20b = Calculator("HP", "20b")
//hp20b: Calculator = Calculator(hp,20b)

val hp20B = Calculator("HP", "20b")
//hp20B: Calculator = Calculator(hp,20b)

hp20b == hp20B
//res6: Boolean = true


//CASE CLASSES WITH PATTERN MATCHING
val hp20b = Calculator("HP", "20B")
val hp30b = Calculator("HP", "30B")

def calcType(calc: Calculator) = calc match {
  case Calculator("HP", "20B") => "financial"
  case Calculator("HP", "48G") => "scientific"
  case Calculator("HP", "30B") => "business"
  case Calculator(ourBrand, ourModel) => "Calculator: %s %s is of unknown type".format(ourBrand, ourModel)
}


/**
Other alternatives for that last match

  case Calculator(_, _) => "Calculator of unknown type"

OR we could simply not specify that it’s a Calculator at all.
  case _ => "Calculator of unknown type"

OR we could re-bind the matched value with another name
  case c@Calculator(_, _) => "Calculator: %s of unknown type".format(c)
 */

/**
Exceptions
try {
  remoteCalculatorService.add(1, 2)
} catch {
  case e: ServerIsDownException => log.error(e, "the remote calculator service is unavailable. should have kept your trusty HP.")
} finally {
  remoteCalculatorService.close()
}

trys are also expression-oriented

 val result: Int = try {
  remoteCalculatorService.add(1, 2)
} catch {
  case e: ServerIsDownException => {
    log.error(e, "the remote calculator service is unavailable. should have kept your trusty HP.")
    0
  }
} finally {
  remoteCalculatorService.close()
}
 */
