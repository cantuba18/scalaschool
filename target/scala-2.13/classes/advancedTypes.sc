//View bounds (“type classes”)
implicit def strToInt(x: String) = x.toInt
//strToInt: (x: String)Int

"123"
//res0: java.lang.String = 123

val y: Int = "123"
//y: Int = 123

math.max("123", 111)
//res1: Int = 123

class Container[A <% Int] { def addIt(x: A) = 123 + x }
//defined class Container

(new Container[String]).addIt("123")
//res11: Int = 246

(new Container[Int]).addIt(123)
//res12: Int = 246

/**
scala> (new Container[Float]).addIt(123.2F)

<console>:8: error: could not find implicit value for evidence parameter of type (Float) => Int
  (new Container[Float]).addIt(123.2)
 */


//Other type bounds
class Container[A](value: A) { def addIt(implicit evidence: A =:= Int) = 123 + value }
//defined class Container

(new Container(123)).addIt
//res11: Int = 246

/**
(If you get errors trying to use <:< or <%<, be aware that those went away in Scala 2.10. Scala School examples work with Scala 2.9.x . You can use a newer Scala, but expect errors.)

(new Container("123")).addIt

<console>:10: error: could not find implicit value for parameter evidence: =:=[java.lang.String,Int]

class Container[A](value: A) { def addIt(implicit evidence: A <%< Int) = 123 + value }
//defined class Container

(new Container("123")).addIt
//res15: Int = 246
*/

/**
Generic programming with views

def min[B >: A](implicit cmp: Ordering[B]): A = {
  if (isEmpty)
    throw new UnsupportedOperationException("empty.min")

  reduceLeft((x, y) => if (cmp.lteq(x, y)) x else y)
}
*/

List(1,2,3,4).min
//res0: Int = 1

List(1,2,3,4).min(new Ordering[Int] { def compare(a: Int, b: Int) = b compare a })
//res3: Int = 4

trait LowPriorityOrderingImplicits {
  implicit def ordered[A <: Ordered[A]]: Ordering[A] = new Ordering[A] {
    def compare(x: A, y: A) = x.compare(y)
  }
}

//Context bounds & implicitly[]
def foo[A](implicit x: Ordered[A]) {}
//foo: [A](implicit x: Ordered[A])Unit

def foo[A : Ordered] {}
//foo: [A](implicit evidence$1: Ordered[A])Unit

implicitly[Ordering[Int]]
//res37: Ordering[Int] = scala.math.Ordering$Int$@3a9291cf


//Higher-kinded types & ad-hoc polymorphism
trait Container[M[_]] { def put[A](x: A): M[A]; def get[A](m: M[A]): A }

val container = new Container[List] { def put[A](x: A) = List(x); def get[A](m: List[A]) = m.head }
//container: java.lang.Object with Container[List] = $anon$1@7c8e3f75

container.put("hey")
//res24: List[java.lang.String] = List(hey)

container.put(123)
//res25: List[Int] = List(123)

trait Container[M[_]] { def put[A](x: A): M[A]; def get[A](m: M[A]): A }

implicit val listContainer = new Container[List] { def put[A](x: A) = List(x); def get[A](m: List[A]) = m.head }

implicit val optionContainer = new Container[Some] { def put[A](x: A) = Some(x); def get[A](m: Some[A]) = m.get }

def tupleize[M[_]: Container, A, B](fst: M[A], snd: M[B]) = {
   val c = implicitly[Container[M]]
   c.put(c.get(fst), c.get(snd))
   }
//tupleize: [M[_],A,B](fst: M[A],snd: M[B])(implicit evidence$1: Container[M])M[(A, B)]

tupleize(Some(1), Some(2))
//res33: Some[(Int, Int)] = Some((1,2))

tupleize(List(1), List(2))
//res34: List[(Int, Int)] = List((1,2))



//Structural types
def foo(x: { def get: Int }) = 123 + x.get
//foo: (x: AnyRef{def get: Int})Int

foo(new { def get = 10 })
//res0: Int = 133


//Abstract type members
trait Foo { type A; val x: A; def getX: A = x }
//defined trait Foo

(new Foo { type A = Int; val x = 123 }).getX
//res3: Int = 123

(new Foo { type A = String; val x = "hey" }).getX
//res4: java.lang.String = hey

trait Foo[M[_]] { type t[A] = M[A] }
//defined trait Foo

val x: Foo[List]#t[Int] = List(1)
//x: List[Int] = List(1)

//Type erasures & manifests
class MakeFoo[A](implicit manifest: Manifest[A]) { def make: A = manifest.erasure.newInstance.asInstanceOf[A] }

(new MakeFoo[String]).make
//res10: String = ""



//F-bounded polymorphism
trait Container extends Ordered[Container]

//However, this now necessitates the subclass to implement the compare method
def compare(that: Container): Int

/**
class MyContainer extends Container {
  def compare(that: MyContainer): Int
}

trait Container[A] extends Ordered[A]

class MyContainer extends Container[MyContainer] {
  def compare(that: MyContainer) = 0
}

List(new MyContainer, new MyContainer, new MyContainer)
//res3: List[MyContainer] = List(MyContainer@30f02a6d, MyContainer@67717334, MyContainer@49428ffa)

scala> List(new MyContainer, new MyContainer, new MyContainer).min
//res4: MyContainer = MyContainer@33dfeb30


class YourContainer extends Container[YourContainer] { def compare(that: YourContainer) = 0 }
//defined class YourContainer

List(new MyContainer, new MyContainer, new MyContainer, new YourContainer)
//res2: List[Container[_ >: YourContainer with MyContainer <: Container[_ >: YourContainer with MyContainer <: ScalaObject]]]
//= List(MyContainer@3be5d207, MyContainer@6d3fe849, MyContainer@7eab48a7, YourContainer@1f2f0ce9)

 (new MyContainer, new MyContainer, new MyContainer, new YourContainer).min

<console>:9: error: could not find implicit value for parameter cmp:
  Ordering[Container[_ >: YourContainer with MyContainer <: Container[_ >: YourContainer with MyContainer <: ScalaObject]]]
 */


/**
//Case study: Finagle
trait Service[-Req, +Rep] extends (Req => Future[Rep])

trait Filter[-ReqIn, +RepOut, +ReqOut, -RepIn]
  extends ((ReqIn, Service[ReqOut, RepIn]) => Future[RepOut])
{
  def andThen[Req2, Rep2](next: Filter[ReqOut, RepIn, Req2, Rep2]) =
    new Filter[ReqIn, RepOut, Req2, Rep2] {
      def apply(request: ReqIn, service: Service[Req2, Rep2]) = {
        Filter.this.apply(request, new Service[ReqOut, RepIn] {
          def apply(request: ReqOut): Future[RepIn] = next(request, service)
          override def release() = service.release()
          override def isAvailable = service.isAvailable
        })
      }
    }

  def andThen(service: Service[ReqOut, RepIn]) = new Service[ReqIn, RepOut] {
    private[this] val refcounted = new RefcountedService(service)

    def apply(request: ReqIn) = Filter.this.apply(request, refcounted)
    override def release() = refcounted.release()
    override def isAvailable = refcounted.isAvailable
  }
}

trait RequestWithCredentials extends Request {
  def credentials: Credentials
}

class CredentialsFilter(credentialsParser: CredentialsParser)
  extends Filter[Request, Response, RequestWithCredentials, Response]
{
  def apply(request: Request, service: Service[RequestWithCredentials, Response]): Future[Response] = {
    val requestWithCredentials = new RequestWrapper with RequestWithCredentials {
      val underlying = request
      val credentials = credentialsParser(request) getOrElse NullCredentials
    }

    service(requestWithCredentials)
  }
}

val upFilter =
  logTransaction     andThen
    handleExceptions   andThen
    extractCredentials andThen
    homeUser           andThen
    authenticate       andThen
    route
 */