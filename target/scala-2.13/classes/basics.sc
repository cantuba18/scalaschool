//Expressions
1 + 1
//res0: Int = 2


//Values
val two = 1 + 1
//two: Int = 2


//Variables
var name = "steve"
//name: java.lang.String = steve

name = "marius"
//name: java.lang.String = marius


//Functions
def addOne(m: Int): Int = m + 1
//addOne: (m: Int)Int
val three = addOne(2)
//three: Int = 3

def three() = 1 + 2
//three: ()Int

three()
//res2: Int = 3

three
//res3: Int = 3


//Anonymous Functions
(x: Int) => x + 1
//(Int) => Int = <function1>

res3(1)
//res4: Int = 2

val addOne = (x: Int) => x + 1
//addOne: (Int) => Int = <function1>

  addOne(1)
//res5: Int = 2

def timesTwo(i: Int): Int = {
  println("hello world")
  i * 2
}

{ i: Int =>
  println("hello world")
  i * 2
}
//res6: (Int) => Int = <function1>

//Partial application
def adder(m: Int, n: Int) = m + n
//adder: (m: Int,n: Int)Int

val add2 = adder(2, _:Int)
//add2: (Int) => Int = <function1>

  add2(3)
 // res7: Int = 5


//Curried functions
def multiply(m: Int)(n: Int): Int = m * n
//multiply: (m: Int)(n: Int)Int

multiply(2)(3)
//res8: Int = 6

val timesTwo = multiply(2) _
//timesTwo: (Int) => Int = <function1>

timesTwo(3)
//res9: Int = 6

val curriedAdd = (adder _).curried
//curriedAdd: Int => (Int => Int) = <function1>

val addTwo = curriedAdd(2)
//addTwo: Int => Int = <function1>

addTwo(4)
//res10: Int = 6


//Variable length arguments
def capitalizeAll(args: String*) = {
  args.map { arg =>
    arg.capitalize
  }
}

capitalizeAll("rarity", "applejack")
//res11: Seq[String] = ArrayBuffer(Rarity, Applejack)


//Classes
class Calculator {
    val brand: String = "HP"
     def add(m: Int, n: Int): Int = m + n
   }
//defined class Calculator

val calc = new Calculator
//calc: Calculator = Calculator@e75a11

calc.add(1, 2)
//res12: Int = 3

calc.brand
//res13: String = "HP"


//Constructor
class Calculator(brand: String) {
  /**
   * A constructor.
   */
  val color: String = if (brand == "TI") {
    "blue"
  } else if (brand == "HP") {
    "black"
  } else {
    "white"
  }

  // An instance method.
  def add(m: Int, n: Int): Int = m + n
}

val calc = new Calculator("HP")
//calc: Calculator = Calculator@1e64cc4d

calc.color
//res14: String = black


//Expressions
//Aside: Functions vs Methods
class C {
     var acc = 0
     def minc = { acc += 1 }
     val finc = { () => acc += 1 }
   }
//defined class C

val c = new C
//c: C = C@1af1bd6

c.minc // calls c.minc()

c.finc // returns the function as a value:
//res16: () => Unit = <function0>


//Inheritance
class ScientificCalculator(brand: String) extends Calculator(brand) {
  def log(m: Double, base: Double) = math.log(m) / math.log(base)
}

//Overloading methods
class EvenMoreScientificCalculator(brand: String) extends ScientificCalculator(brand) {
  def log(m: Int): Double = log(m, math.exp(1))
}


//Abstract Classes
abstract class Shape {
    def getArea():Int    // subclass should define this
   }
//defined class Shape

class Circle(r: Int) extends Shape {
     def getArea():Int = { r * r * 3 }
   }
//defined class Circle

/**
val s = new Shape
<console>:8: error: class Shape is abstract; cannot be instantiated
val s = new Shape
 */

val c = new Circle(2)
// c: Circle = Circle@65c0035b


//Traits
trait Car {
  val brand: String
}

trait Shiny {
  val shineRefraction: Int
}

class BMW extends Car {
  val brand = "BMW"
}

class BMW extends Car with Shiny {
  val brand = "BMW"
  val shineRefraction = 12
}


//Types
trait Cache[K, V] {
  def get(key: K): V
  def put(key: K, value: V)
  def delete(key: K)
}

def remove[K](key: K)
